﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMMS_Bridge.DTOs
{
    public class ServivceRequestDto
    {
        public string VDOT_SRID { get; set; }
        public string VW_SRID { get; set; }
        public string Status { get; set; }
        public DateTime TimeStampSent { get; set; }
        public DateTime TimeStampReceived { get; set; }
    }
}
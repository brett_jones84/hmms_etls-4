declare @ServiceRequestID int

insert into dbo.Comment (WorkOrderID, CommentText, CommentType, CommentCreationDate, CommentUpdateDate)
values (123,'CommentText', 'CommentType', GETDATE(), GETDATE()) 

@ServiceRequestID = (select SCOPE_IDENTITY())

insert into dbo.ServiceRequest(ServiceRequestID, ServiceRequest_VW_ID, [Status], TimeStampSent, TimeStampReceived) 
values (@ServiceRequestID ,2, 'GOOD', GETDAte(), GETDATE())
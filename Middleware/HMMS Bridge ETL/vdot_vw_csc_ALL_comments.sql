USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_ALL_comments') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_ALL_comments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_csc_ALL_comments] AS   
WITH src as (
	select 
		[AnnotationId]
      ,[CSC_SR_ID]  COLLATE sql_latin1_general_cp1_ci_as AS CSC_SR_ID
      ,CAST([HMMS_SR_ID] AS nvarchar(100)) as HMMS_SR_ID
      ,[subject]
      ,[NoteText] COLLATE sql_latin1_general_cp1_ci_as AS NoteText
      ,[modifiedOn]
      ,[duplicateCount]
      ,[parent_ID]
	   from [vdot_vw_csc_ALL_comments_SR]
	UNION ALL
	select 
	[AnnotationId]
      ,[CSC_SR_ID]
      ,CAST([HMMS_SR_ID] AS nvarchar(100)) as HMMS_SR_ID
      ,[subject]
      ,[NoteText]
      ,[modifiedOn]
      ,[duplicateCount]
      ,[parent_ID]
	 from [vdot_vw_csc_ALL_comments_DUPS]
	)
	SELECT * FROM src	



GO
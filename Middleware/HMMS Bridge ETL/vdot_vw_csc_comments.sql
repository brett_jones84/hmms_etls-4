USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_comments') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_comments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vdot_vw_csc_comments] AS 

  SELECT       
            AnnotationId
            ,csc.CSC_SR_ID  
            ,csc.HMMS_SR_ID
            ,subject
            ,NoteText
            ,modifiedOn
            ,duplicateCount 
  FROM   vdot_vw_csc_ALL_comments AS csc
         LEFT JOIN [dbo].[vdot_vw_sr_ALL_comments] vw 
                ON csc.CSC_SR_ID = vw.csc_sr_id COLLATE 
                                   sql_latin1_general_cp1_ci_as 
                   AND csc.notetext = vw.comment COLLATE 
                                      sql_latin1_general_cp1_ci_as 
		INNER JOIN reports.ServiceRequests as hmms_sr 
				on hmms_sr.ID = csc.HMMS_SR_ID COLLATE 
                                      sql_latin1_general_cp1_ci_as
					AND 
					hmms_sr.RefID = csc.CSC_SR_ID COLLATE 
                                      sql_latin1_general_cp1_ci_as
  WHERE  csc.notetext IS NOT NULL 
         AND vw.comment IS NULL 
        AND hmms_sr.RefID IS NOT NULL	
		 --AND ISNULL(csc.Subject,'') not in ('Comment from AMS','Cancellation Reason', 'Closed Status Reason (To Customer)','Open Status Reason (To Customer)','Response to Customer: (Optional)')
		 


; 


















GO














GO



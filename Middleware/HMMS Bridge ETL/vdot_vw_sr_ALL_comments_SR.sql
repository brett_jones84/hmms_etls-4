USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_sr_ALL_comments_SR') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_sr_ALL_comments_SR]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_sr_ALL_comments_SR] AS
SELECT 
	c.ID + ABS(CHECKSUM(NewId())) % 100000  AS ID
	,i.Incident_ID AS HMMS_SR_ID
	,i.ref_id AS CSC_SR_ID
	,NULL AS WorkOrderID
	,'Internal Comment from HMMS Service Request' AS CommentType
	,c.Comment  AS Comment
	,c.CreationDate AS CommentCreationDate
	,c.UpdateDate AS CommentUpdateDate
	, 0 as duplicate
	, '' as parentHMMSID
	, '' as parentCSCID
FROM dbo.tbl_Comments C
JOIN dbo.tbl_Incident_Reports I ON I.Incident_ID = c.Ref_ID 
WHERE c.RefType = 0
AND i.ref_id IS NOT NULL


GO
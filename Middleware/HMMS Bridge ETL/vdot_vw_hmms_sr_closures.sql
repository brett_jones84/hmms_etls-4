USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_sr_closures') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_sr_closures]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[vdot_vw_hmms_sr_closures] as
SELECT TOP 50 
	id HMMS_SR_ID
	, refid CSC_SR_ID
	, ClosedDate
	, ClosedByName
	--hardcoded in for IIB integration.
	, 'CLOSED' AS  StatusName

	--add in a STATUS_NAME = 'CLOSED' as a constant.

  FROM [$(MAIN_DB)].[reports].[ServiceRequests]
  --past 5 days
  where closedDate > Dateadd(dd, -5, Getutcdate()) 



GO


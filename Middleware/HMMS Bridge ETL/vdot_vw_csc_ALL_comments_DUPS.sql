USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_ALL_comments_DUPS') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_ALL_comments_DUPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 CREATE VIEW [dbo].[vdot_vw_csc_ALL_comments_DUPS] AS 
select 	
	cscComments.[AnnotationId]
	,sr.RefID as CSC_SR_ID
	,dups.[parent_ID] AS [HMMS_SR_ID]
	,cscComments.[subject]
	--,'Comment From Duplicate CSC Service Request - '+ CAST(dups.CSC_SR_ID COLLATE sql_latin1_general_cp1_ci_as  as nvarchar(100)) + CHAR(13) + CHAR(10)  + cscComments.[NoteText] AS NoteText
	,cscComments.[NoteText]
	,cscComments.[modifiedOn]
	,cscComments.[duplicateCount]
	,dups.parent_ID
	
from vdot_vw_csc_ALL_comments_SR cscComments
left join vdot_vw_hmms_duplicates as dups on 
	cscComments.HMMS_SR_ID = dups.HMMS_SR_ID
left join reports.ServiceRequests as sr on
	dups.parent_ID = sr.ID
where 
	dups.parent_ID is not null









GO




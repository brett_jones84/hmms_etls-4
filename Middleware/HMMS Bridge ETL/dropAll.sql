USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_A21]
GO

IF OBJECT_ID('vdot_vw_A21_ChartFieldNumbers') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_A21_ChartFieldNumbers]
GO

IF OBJECT_ID('vdot_vw_A21_departments') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_A21_departments]
GO

IF OBJECT_ID('vdot_vw_A21_Inventory') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_A21_Inventory]
GO

IF OBJECT_ID('vdot_vw_A21_Labor') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_A21_Labor]
GO

IF OBJECT_ID('vdot_vw_A21_Leave') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_A21_Leave]
GO

IF OBJECT_ID('vdot_vw_csc_ALL_comments') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_ALL_comments]
GO

IF OBJECT_ID('vdot_vw_csc_all_SRs') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_all_SRs]
GO

IF OBJECT_ID('vdot_vw_csc_comments') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_comments]
GO

IF OBJECT_ID('vdot_vw_csc_districts') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_districts]
GO

IF OBJECT_ID('vdot_vw_csc_issues') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_issues]
GO

IF OBJECT_ID('vdot_vw_csc_new_SRs') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_new_SRs]
GO

IF OBJECT_ID('vdot_vw_csc_pendingCancelSRs') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_pendingCancelSRs]
GO

IF OBJECT_ID('vdot_vw_csc_residencies') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_residencies]
GO

IF OBJECT_ID('vdot_vw_csc_SRs_No_WOID') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_SRs_No_WOID]
GO

IF OBJECT_ID('vdot_vw_csc_workAreas') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_csc_workAreas]
GO

IF OBJECT_ID('vdot_vw_hmms_changedWorkAreas') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_hmms_changedWorkAreas]
GO

IF OBJECT_ID('vdot_vw_hmms_duplicates') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_hmms_duplicates]
GO

IF OBJECT_ID('vdot_vw_hmms_sr_closures') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_hmms_sr_closures]
GO

IF OBJECT_ID('vdot_vw_hmms_sr_status_changes') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_hmms_sr_status_changes]
GO

IF OBJECT_ID('vdot_vw_master_comments') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_master_comments]
GO

IF OBJECT_ID('vdot_vw_sr_ALL_comments') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_sr_ALL_comments]
GO

IF OBJECT_ID('vdot_vw_sr_comments') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_sr_comments]
GO

IF OBJECT_ID('vdot_vw_sr_comments_NEW') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_sr_comments_NEW]
GO

IF OBJECT_ID('vdot_vw_stock_locations') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_stock_locations]
GO

IF OBJECT_ID('vdot_vw_test_sr') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_test_sr]
GO

IF OBJECT_ID('vdot_vw_TMOCMART_Function') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_TMOCMART_Function]
GO

IF OBJECT_ID('vdot_vw_webims_staged_inv_at_location') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_webims_staged_inv_at_location]
GO

IF OBJECT_ID('vdot_vw_webims_staged_inventory') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_webims_staged_inventory]
GO

IF OBJECT_ID('vdot_vw_wo_ALL_toCustomer') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_wo_ALL_toCustomer]
GO

IF OBJECT_ID('vdot_vw_wo_toCustomer') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_wo_toCustomer]
GO

IF OBJECT_ID('vdot_vw_wo_toCustomer_NEW') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_wo_toCustomer_NEW]
GO

IF OBJECT_ID('vdot_vw_WorkOrder_Details') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_WorkOrder_Details]
GO

IF OBJECT_ID('vdot_vw_xref_nigp_webims') IS NOT NULL
	DROP VIEW [dbo].[vdot_vw_xref_nigp_webims]
GO



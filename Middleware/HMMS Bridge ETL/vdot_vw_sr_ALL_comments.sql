USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_sr_ALL_comments') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_sr_ALL_comments]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_sr_ALL_comments] AS
	SELECT * FROM vdot_vw_sr_ALL_comments_SR
	UNION ALL
	SELECT * FROM vdot_vw_sr_ALL_comments_WO
	UNION ALL
	SELECT * FROM vdot_vw_sr_ALL_comments_Dups

GO
USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_issues') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_issues]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_csc_issues] as
SELECT [vdot_name]
      ,[vdot_AMSCode]
      ,[vdot_SortOrder]
      ,[vdot_TalkingPoint]
  FROM [$(MSCRM)].[dbo].[vdot_problemtype] with (NOLOCK)
  where vdot_AMSCode is not null;
GO
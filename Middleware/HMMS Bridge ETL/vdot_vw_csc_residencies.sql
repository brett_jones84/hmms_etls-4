USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_residencies') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_residencies]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--residency/district
CREATE VIEW [dbo].[vdot_vw_csc_residencies] AS
  SELECT r.[vdot_ResidencyCode] residencyCode
	  , r.[vdot_name] residencyName
      , r.[EmailAddress] residencyEmail
      , r.[vdot_ContactName] residencyContactName
      , r.[vdot_ContactPhone] residencyContactPhone
      , r.[vdot_ResidencyAddress] residencyAddress
      , r.[vdot_District] districtID
	  , d.vdot_name districtName
	  , d.vdot_ContactPhone districtPhone
  FROM [$(MSCRM)].[dbo].[vdot_residency] r with (NOLOCK)
  LEFT JOIN [$(MSCRM)].[dbo].[vdot_district] d
	on d.vdot_districtId = r.vdot_District;



GO

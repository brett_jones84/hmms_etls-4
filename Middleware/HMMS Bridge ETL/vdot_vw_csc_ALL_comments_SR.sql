USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_ALL_comments_SR') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_ALL_comments_SR]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_csc_ALL_comments_SR] AS 
  WITH src 		
       AS (SELECT 
          a.AnnotationId,
				  sr.vdot_srid,            
				  sr.vdot_HMMSIntegrationID,
          a.subject, 
          a.objecttypecode, 
          a.isdocument, 
          a.modifiedon,                   
          --get rid of all whitespace at the beginning and end of the string - 
        --HMMS does this when it stores comments and CSC does not so we need to true it up here.             
				   RTRIM(
						LTRIM(
							RIGHT(
								LEFT(NoteText,LEN(NoteText)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(NoteText))+1)
							,LEN(
								LEFT(NoteText,LEN(NoteText)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(NoteText))+1)
							)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',
								LEFT(NoteText,LEN(NoteText)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(NoteText))+1))+1)
						)
					) as NoteText         
          FROM   $(MSCRM).dbo.annotation a with (NOLOCK)
          INNER JOIN $(MSCRM).dbo.vdot_servicerequest sr 
            ON a.objectid = sr.vdot_servicerequestid 
          WHERE  
          -- a.modifiedon > Dateadd(dd, -5, Getutcdate()) 
          -- AND 
          sr.vdot_HMMSIntegrationID IS NOT NULL 
           
           UNION 
		   
           SELECT 
            sr.vdot_servicerequestId as [AnnotationId],
				    sr.vdot_srid,            
				    sr.vdot_HMMSIntegrationID, 
            'Comment from AMS' AS [Subject], 
            10020              AS [ObjectTypeCode], 
            0                  AS [IsDocument], 
            NULL, 
        --get rid of all whitespace at the beginning and end of the string - 
				--HMMS does this when it stores comments and CSC does not so we need to true it up here.             
				   RTRIM(
						LTRIM(
							RIGHT(
								LEFT(vdot_comments,LEN(vdot_comments)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(vdot_comments))+1)
							,LEN(
								LEFT(vdot_comments,LEN(vdot_comments)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(vdot_comments))+1)
							)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',
								LEFT(vdot_comments,LEN(vdot_comments)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(vdot_comments))+1))+1)
						)
					) as NoteText      
           FROM   $(MSCRM).dbo.vdot_servicerequest sr with (NOLOCK)
           WHERE  
          --  sr.modifiedon > Dateadd(dd, -5, Getutcdate()) 
          --  AND 
           sr.vdot_HMMSIntegrationID IS NOT NULL), 
       csc 	   
       AS (SELECT 
          AnnotationId,
				  vdot_srid,                   
				  vdot_HMMSIntegrationID, 
          subject, 
          notetext, 
          Min(modifiedon) modifiedOn, 
          Count(*)        duplicateCount 
          FROM   src 		   
          GROUP  BY 
             AnnotationId,
             vdot_srid,                      
             vdot_HMMSIntegrationID,
             subject, 
             notetext
           ) 
  
          SELECT 
            AnnotationId
            ,vdot_srid as CSC_SR_ID  
            ,vdot_HMMSIntegrationID as HMMS_SR_ID
            ,subject
            ,NoteText
            ,modifiedOn
            ,duplicateCount
			, NULL AS parent_ID
          FROM   csc 
  

; 


















GO



USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_duplicates_rollup') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_duplicates_rollup]
GO

CREATE VIEW [dbo].[vdot_vw_hmms_duplicates_rollup] AS
Select distinct dups2.parent_ID, 
    substring(
        (
            Select ','+CHAR(10)+dups1.CSC_SR_ID
            From dbo.vdot_vw_hmms_duplicates dups1
			left join dbo.vdot_vw_csc_all_SRs as cscSR on dups1.CSC_SR_ID = cscSR.vdot_srid COLLATE SQL_Latin1_General_CP1_CI_AS 
            Where dups1.parent_ID = dups2.parent_ID
            ORDER BY dups1.parent_ID
            For XML PATH ('')
        ), 3, 1000) [Duplicates]
From dbo.vdot_vw_hmms_duplicates dups2
where dups2.CSC_SR_ID is not null
GO
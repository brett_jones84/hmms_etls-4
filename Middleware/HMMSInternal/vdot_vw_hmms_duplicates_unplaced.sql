USE [$(MAIN_DB)]
GO
IF OBJECT_ID('vdot_vw_hmms_duplicates_unplaced') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_duplicates_unplaced]
GO


CREATE VIEW [dbo].[vdot_vw_hmms_duplicates_unplaced] AS
with src as (
SELECT 
	ID
	,[Duplicate CSC SR IDs]
	,[Duplicate CSC SR Callers]
 FROM reports.WorkOrderForm_MaintenanceLinearWoForm as lwo

 UNION ALL

SELECT 
	ID
	,[Duplicate CSC SR IDs]
	,[Duplicate CSC SR Callers]
FROM reports.WorkOrderForm_MaintenanceWorkOrderForm as dwo

)
SELECT 
	src.ID as WO_ID	
	,wo.WorkOrderID
	,wo.ParentServiceRequestID	
	,src.[Duplicate CSC SR IDs]	
	, dups.Duplicates
FROM src
left join reports.workorders as wo on src.ID = wo.ID
join dbo.vdot_vw_hmms_duplicates_rollup as dups on wo.ParentServiceRequestID = dups.parent_ID
WHERE 
	dups.Duplicates IS NOT NULL
	AND
	dups.Duplicates <> src.[Duplicate CSC SR IDs]




GO

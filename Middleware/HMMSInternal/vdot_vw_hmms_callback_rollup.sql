USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_callback_rollup') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_callback_rollup]
GO

CREATE VIEW [dbo].[vdot_vw_hmms_callback_rollup] AS
WITH src AS (
select 
	 cscSR.vdot_CustomerName
	 , hmmsSR.ServiceRequestID
	, CASE WHEN dups.parent_ID IS NULL 
		THEN	
			''
		ELSE
			' - Duplicate'
		END AS vdot_CallBack_Type	
	,COALESCE(dups.parent_ID, cscSR.vdot_HMMSIntegrationID) as vdot_HMMSIntegrationID
	, (
			'('
			+SUBSTRING(cscSR.vdot_Phone1, 1, 3) 
			+ '-' 
			+ SUBSTRING(cscSR.vdot_Phone1, 4, 3) 
			+ '-' 
			+ SUBSTRING(cscSR.vdot_Phone1, 7, 4) 
			+ ')'
		)		as phone
from vdot_vw_csc_all_SRs cscSR
	left join 
		dbo.vdot_vw_hmms_duplicates dups 
		on 
		cscSR.vdot_srid = dups.CSC_SR_ID COLLATE Latin1_General_CI_AI 
	left join
		reports.ServiceRequests AS hmmsSR
		on
		cscSR.vdot_HMMSIntegrationID = hmmsSR.ID
	
where 
	cscSR.vdot_callbackreq = 1
	and
	cscSR.vdot_HMMSIntegrationID is not null

)



Select distinct src2.vdot_HMMSIntegrationID
			,
			substring(
			(
			Select ','+CHAR(10)+src1.ServiceRequestID + ' - -' +src1.vdot_CustomerName +' '+src1.phone +  src1.vdot_CallBack_Type
	            From src as src1
				Where src1.vdot_HMMSIntegrationID = src2.vdot_HMMSIntegrationID
		        ORDER BY src1.vdot_HMMSIntegrationID
			    For XML PATH ('') ), 3, 1000) 
				as Callbacks
from src as src2

GO



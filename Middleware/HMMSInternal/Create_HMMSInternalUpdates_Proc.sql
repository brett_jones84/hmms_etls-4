USE $(MAIN_DB)
GO

-- PRINT '$(MAIN_DB)'
-- PRINT '$(STAGING_DB)'
-- PRINT '$(MSCRM)'

/****** 
--		Object:  	StoredProcedure [dbo].[HMMSInternalUpdates]    
--		Author: 	Michael Kolonay (WorldView Solutions)
--		History: 	Version 1.0,	2/23/2018, 	Michael Kolonay
--					Initial creation of procedure for updating Work order form fields.
******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('HMMSInternalUpdates') IS NOT NULL
	BEGIN
		PRINT N'Dropping existing HMMSInternalUpdates Procedure...';  
		DROP PROCEDURE  [dbo].[HMMSInternalUpdates];
	END
GO

--PRINT N'Creating LaborDataLoad Procedure...';  
CREATE procedure [dbo].[HMMSInternalUpdates] 	
as
begin

PRINT N'HMMSInternalUpdates Procedure Beginning...';  
BEGIN TRANSACTION [Tran1]

BEGIN TRY


	-----------------------------------------------------------------
	-----------------  Update 'Duplicate CSC SR Callers' field  in two forms-----------------
	-----------------  Maintenance Linear WO Form 94-----------------
	-----------------  Maintenance Work Order Form 93-----------------	
	-----------------------------------------------------------------

-- Table: dbo.tbl_Maintenance_Activity_ExData_Config93
-- Column: MultiLine2
	merge dbo.tbl_Maintenance_Activity_ExData_Config93 as Target
	using (
		SELECT	
		s.[WO_ID]
      ,s.[Callbacks]
		FROM [dbo].[vdot_vw_hmms_callback_unplaced] s
	) AS Source
	on Target.Activity_ID = Source.WO_ID
	when matched then
		update set 
				Target.MultiLine2 = source.Callbacks;

-- Table: dbo.tbl_Maintenance_Activity_ExData_Config94
-- Column: MultiLine1
	merge dbo.tbl_Maintenance_Activity_ExData_Config94 as Target
	using (
		SELECT	
		s.[WO_ID]
      ,s.[Callbacks]
		FROM [dbo].[vdot_vw_hmms_callback_unplaced] s
	) AS Source
	on Target.Activity_ID = Source.WO_ID
	when matched then
		update set 
				Target.MultiLine1 = source.Callbacks;




	-----------------------------------------------------------------
	-----------------  Update 'Duplicate CSC SR ID's' field  in two forms -----------------
	-----------------  Maintenance Linear WO Form 94----------------
	-----------------  Maintenance Work Order Form 93-----------------
	-----------------------------------------------------------------
-- Table: dbo.tbl_Maintenance_Activity_ExData_Config93
-- Column: MultiLine1
	merge dbo.tbl_Maintenance_Activity_ExData_Config93 as Target
	using (
		SELECT	
		s.[WO_ID]
      ,s.[Duplicates]
		FROM [dbo].[vdot_vw_hmms_duplicates_unplaced] s
	) AS Source
	on Target.Activity_ID = Source.WO_ID
	when matched then
		update set 
				Target.MultiLine1 = source.Duplicates;


-- Table: dbo.tbl_Maintenance_Activity_ExData_Config88
-- Column: MultiLine2
	merge dbo.tbl_Maintenance_Activity_ExData_Config94 as Target
	using (
		SELECT	
		s.[WO_ID]
      ,s.[Duplicates]
		FROM [dbo].[vdot_vw_hmms_duplicates_unplaced] s
	) AS Source
	on Target.Activity_ID = Source.WO_ID
	when matched then
		update set 
				Target.MultiLine2 = source.Duplicates;





	PRINT N'Committing transaction...'; 
	COMMIT TRANSACTION [Tran1];

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION [Tran1];

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

	--TODO: email?

	-- return error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
END CATCH  

PRINT N'...HMMSInternalUpdates Procedure Complete'; 

END
 

 
GO



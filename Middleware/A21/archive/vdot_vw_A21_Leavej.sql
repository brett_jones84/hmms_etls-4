USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21_Leave') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21_Leave]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













CREATE VIEW [dbo].[vdot_vw_A21_Leave] AS
		SELECT wo.ID
			, wo.WorkOrderID AS Work_Order_Id
			,'' AS Is_Disaster
			, '' AS Speedtype
			, CAST(wo.BeginDate AS Date) AS Date_Of_Work
			, SUBSTRING(wolf.[Department ID], 1, 5) AS Department_ID
			, wolf.[Department ID] AS Dept_Description
			,'' AS Cost_Center_Code
			, '' AS Project
			, '' AS Task_Code
			, '' AS Task_Description
			, '' AS Activity_Code			
			, wo.ActivityDescription AS Activity_Description
			, '' AS FIPS 
			, '' AS Agency1__AU
			, '' AS Agency2__AU2
			, '' as Asset
			--the rest of the view changes based on what type of resource you are looking at.
			, 'LABOR' AS Resource_Type
			, '' AS Resource
			, ru.RateName AS Time_Recording_Charge --this needs to be fixed...
			, ru.EmployeeID as Employee_ID
			, ru.Hours AS Resource_Quantity
			, 'hours' AS Resource_UOM
			, '' AS Stock_Location
			, '' AS NIGP_CODE
			, '' as Condition
			

FROM 
	reports.WorkOrders AS wo 	
	LEFT JOIN [reports].[WorkOrderForm_MaintenanceLeaveForm] AS wolf ON wo.ID = wolf.ID
	LEFT JOIN reports.ResourceUsage as ru ON wo.ID = ru.Record_ID

where wolf.[Department ID] is not null and ru.EmployeeID != -1
and wo.ActivityDescription = 'Leave Time'



--select * from [reports].[WorkOrderForm_MaintenanceLeaveForm]







GO



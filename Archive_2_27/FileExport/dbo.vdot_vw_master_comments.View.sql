USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_master_comments]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_master_comments] 
AS 
  WITH src 
       --gather everything that is considered a csc "comment"
       AS (SELECT sr.vdot_srid, 
                  sr.vdot_amsid, 
                  a.subject, 
                  a.objecttypecode, 
                  a.isdocument, 
                  a.modifiedon, 
                  a.notetext 
           FROM   csc_mscrm.dbo.annotation a 
                  INNER JOIN csc_mscrm.dbo.vdot_servicerequest sr 
                          ON a.objectid = sr.vdot_servicerequestid 
           WHERE  a.modifiedon > Dateadd(dd, -5, Getutcdate()) 
                  AND sr.vdot_amsid IS NOT NULL 
           UNION 
           SELECT sr.vdot_srid, 
                  sr.vdot_amsid, 
                  'Comment from AMS' AS [Subject], 
                  10020              AS [ObjectTypeCode], 
                  0                  AS [IsDocument], 
                  NULL, 
                  sr.vdot_comments   AS [NoteText] 
           FROM   csc_mscrm.dbo.vdot_servicerequest sr 
           WHERE  sr.modifiedon > Dateadd(dd, -5, Getutcdate()) 
                  AND sr.vdot_amsid IS NOT NULL)
       --
	   -- this removes duplicates
	   --
       , csc 
       AS (SELECT vdot_srid, 
                  vdot_amsid, 
                  subject, 
                  notetext, 
                  Min(modifiedon) modifiedOn, 
                  Count(*)        duplicateCount 
           FROM   src 
           GROUP  BY vdot_srid, 
                     vdot_amsid, 
                     subject, 
                     notetext) 
  SELECT * 
  FROM   csc 
GO

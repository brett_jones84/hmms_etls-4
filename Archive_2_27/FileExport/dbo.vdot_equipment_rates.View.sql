USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_equipment_rates]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_equipment_rates] AS	
		SELECT	s.UNIT_NO
				, 1 Rate_ID
				, 'M5 Rate' Rate_name
				, s.[LEASE_TERM]				
				, CASE WHEN s.[LEASE_TERM] = 'HOUR' THEN 0
						ELSE ((s.[LEASE_RATE]/2080)*12)
					END LEASE_RATE
		FROM [HMMS_STAGING].[dbo].[M5_HMMS_EQUIPMENTS] s
		

GO

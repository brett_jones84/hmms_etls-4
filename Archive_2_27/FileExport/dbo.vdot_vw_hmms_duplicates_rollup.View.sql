USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_hmms_duplicates_rollup]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_hmms_duplicates_rollup] AS
Select distinct dups2.parent_ID, 
    substring(
        (
            Select ','+CHAR(10)+dups1.CSC_SR_ID
            From dbo.vdot_vw_hmms_duplicates dups1
			left join dbo.vdot_vw_csc_all_SRs as cscSR on dups1.CSC_SR_ID = cscSR.vdot_srid COLLATE SQL_Latin1_General_CP1_CI_AS 
            Where dups1.parent_ID = dups2.parent_ID
            ORDER BY dups1.parent_ID
            For XML PATH ('')
        ), 3, 1000) [Duplicates]
From dbo.vdot_vw_hmms_duplicates dups2
where dups2.CSC_SR_ID is not null
GO

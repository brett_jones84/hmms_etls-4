USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_sr_ALL_comments_WO]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_sr_ALL_comments_WO] AS
SELECT 
	c.ID + ABS(CHECKSUM(NewId())) % 10000  as ID
	,i.Incident_ID AS HMMS_SR_ID
	,i.ref_id AS CSC_SR_ID
	,m.Activity_ID  AS WorkOrderID
	,'Work Order' AS CommentType
	,c.Comment AS Comment
	,c.CreationDate AS CommentCreationDate
	,c.UpdateDate  AS CommentUpdateDate
	, 0 as duplicate
	, '' as parentHMMSID
	, '' as parentCSCID
FROM dbo.tbl_Comments C
JOIN dbo.tbl_Maintenance_Activity m ON m.Activity_ID = c.Ref_ID
LEFT JOIN tbl_Incident_Reports i ON i.Incident_ID = m.Incident_ID
WHERE c.RefType = 1
--and c.CreationDate > dateadd(dd,-5,GETUTCDATE())
AND i.ref_id is not null


GO

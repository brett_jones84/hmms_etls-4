USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_WorkOrder_Details]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_WorkOrder_Details] AS
Select 
  wo.ID 
  ,womf.ID as womfID
  ,womlf.ID as womlfID
, COALESCE(womf.[AHQ Name (Base Map)], womlf.[AHQ Name (Base Map)]) as [AHQ Name (Base Map)]
, COALESCE(womf.[Assigned/Pending], womlf.[Assigned/Pending]) as [Assigned/Pending]
, COALESCE(womf.[AU1 (If applicable)], womlf.[AU1 (If applicable)]) as [AU1 (If applicable)]
, COALESCE(womf.[AU2 (Route)], womlf.[AU2 (Route Number)]) as [AU2 (Route)]
, COALESCE(womf.[Cancellation Reason], womlf.[Cancellation Reason]) as [Cancellation Reason]
, COALESCE(womf.[Charge To Department], womlf.[Charge To Department]) as [Charge To Department]
, COALESCE(womf.[City Code (FIPS Code)], womlf.[City Code (FIPS Code)]) as [City Code (FIPS Code)]
, COALESCE(womf.[City Name (Base Map)], womlf.[City Name (Base Map)]) as [City Name (Base Map)]
, COALESCE(womf.[Closed Status Reason (To Customer)], womlf.[Closed Status Reason (To Customer)]) as [Closed Status Reason (To Customer)]
, COALESCE(womf.[County Code (FIPS Code)], womlf.[County Code (FIPS Code)]) as [County Code (FIPS Code)]
, COALESCE(womf.[County Name (Base Map)], womlf.[County Name (Base Map)]) as [County Name (Base Map)]
, COALESCE(womf.[Department ID], womlf.[Department ID]) as [Department ID]
, COALESCE(womf.Disaster, womlf.Disaster) as Disaster
, COALESCE(womf.[District Name (Base Map)], womlf.[District Name (Base Map)]) as [District Name (Base Map)]
, COALESCE(womf.[Federal Structure ID], womlf.[Federal Structure ID]) as [Federal Structure ID]
, COALESCE(womf.[Mile Marker (Base Map)], womlf.[Mile Marker (Base Map)]) as [Mile Marker (Base Map)]
, COALESCE(womf.[Open Status Reason (To Customer)], womlf.[Open Status Reason (To Customer)]) as [Open Status Reason (To Customer)]
, COALESCE(womf.Quantity, womlf.Quantity) as Quantity
, COALESCE(womf.[Residency Name (Base Map)], womlf.[Residency Name (Base Map)]) as [Residency Name (Base Map)]
, COALESCE(womf.[Response to Customer (Optional)], womlf.[Response to Customer (Optional)]) as [Response to Customer (Optional)]
, COALESCE(womf.[State Structure No.], womlf.[State Structure No.]) as [State Structure No.]
, COALESCE(womf.[System], womlf.[System]) as  [System]

, COALESCE(womf.[Town Code (FIPS Code)], womlf.[Town Code (FIPS Code)]) as [Town Code (FIPS Code)]
, COALESCE(womf.[Town Name (Base Map)], womlf.[Town Name (Base Map)]) as [Town Name (Base Map)]
--, COALESCE(womf.[Town Name (FIPS Code)], womlf.[Town Name (FIPS Code)]) as [Town Name (FIPS Code)]
, COALESCE(womf.UOM, womlf.UOM) as UOM
, COALESCE(womf.[UPC Number], womlf.[UPC Number]) as [UPC Number]
, COALESCE(womf.[VDOT City Code (Base Map)], womlf.[VDOT City Code (Base Map)]) as [VDOT City Code (Base Map)]
, COALESCE(womf.[VDOT County Code (Base Map)], womlf.[VDOT County Code (Base Map)]) as [VDOT County Code (Base Map)]
, COALESCE(womf.[VDOT Town Code (Base Map)], womlf.[VDOT Town Code (Base Map)]) as [VDOT Town Code (Base Map)]


from [reports].[WorkOrders] wo
left join reports.WorkOrderForm_MaintenanceWorkOrderForm womf on wo.ID = womf.ID
left join reports.WorkOrderForm_MaintenanceLinearWoForm womlf on wo.ID = womlf.ID

 



GO

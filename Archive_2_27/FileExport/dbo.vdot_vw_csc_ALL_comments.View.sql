USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_ALL_comments]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vdot_vw_csc_ALL_comments] AS 
WITH src as (
	select 
		[AnnotationId]
      ,[CSC_SR_ID]  COLLATE sql_latin1_general_cp1_ci_as AS CSC_SR_ID
      ,CAST([HMMS_SR_ID] AS nvarchar(100)) as HMMS_SR_ID
      ,[subject]
      ,[NoteText] COLLATE sql_latin1_general_cp1_ci_as AS NoteText
      ,[modifiedOn]
      ,[duplicateCount]
      ,[parent_ID]
	   from [vdot_vw_csc_ALL_comments_SR]
	UNION ALL
	select 
	[AnnotationId]
      ,[CSC_SR_ID]
      ,CAST([HMMS_SR_ID] AS nvarchar(100)) as HMMS_SR_ID
      ,[subject]
      ,[NoteText]
      ,[modifiedOn]
      ,[duplicateCount]
      ,[parent_ID]
	 from [vdot_vw_csc_ALL_comments_DUPS]
	)
	SELECT * FROM src	



GO

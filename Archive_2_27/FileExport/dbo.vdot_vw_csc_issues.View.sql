USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_issues]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_csc_issues] as
SELECT [vdot_name]
      ,[vdot_AMSCode]
      ,[vdot_SortOrder]
      ,[vdot_TalkingPoint]
  FROM [CSC_MSCRM].[dbo].[vdot_problemtype] with (NOLOCK)
  where vdot_AMSCode is not null;

GO

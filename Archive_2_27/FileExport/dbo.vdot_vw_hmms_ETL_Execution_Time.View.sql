USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_hmms_ETL_Execution_Time]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_hmms_ETL_Execution_Time] AS

SELECT d.object_id, d.database_id, OBJECT_NAME(object_id, database_id) 'proc name',   
    d.cached_time, d.last_execution_time, d.total_elapsed_time,  
    d.total_elapsed_time/d.execution_count AS [avg_elapsed_time],  
    d.last_elapsed_time, d.execution_count  
FROM sys.dm_exec_procedure_stats AS d  
where  OBJECT_NAME(object_id, database_id) in ('COALoad','IMSDataLoad', 'LaborDataLoad' ) 



GO

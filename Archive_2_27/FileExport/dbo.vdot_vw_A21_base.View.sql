USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_A21_base]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_A21_base] AS
SELECT wo.ID, wo.WorkOrderID AS Work_Order_Id			
			,CASE
				when wodf.Disaster = 1 then 'YES'
				when wodf.Disaster = 0 then 'NO'
				else 'NO'
			end AS Is_Disaster
			, CASE WHEN wo.GroupID = 147 THEN 'Projects' ELSE 'CSC' END AS Speedtype
			, CAST(wo.BeginDate AS Date) AS Date_Of_Work
			, SUBSTRING(wodf.[Department ID], 1, 5) AS Department_ID			
			, wodf.[Department ID] AS Dept_Description
			, wodf.[Charge To Department]
			, SUBSTRING(wodf.[Charge To Department], 1, 5) AS [Charge To Department_ID]			
			,CASE WHEN wo.GroupID = 147 THEN '0' ELSE CONCAT(SUBSTRING(wo.DepartmentName, 1, 3), SUBSTRING(wodf.[System], 1, 1), SUBSTRING(wo.GroupName, 1, 4)) 
			END AS Cost_Center_Code
			, CASE WHEN wo.GroupID = 147 THEN wodf.[UPC Number] ELSE '' END AS Project
			, CASE WHEN wo.GroupID = 147 THEN '-1' ELSE SUBSTRING(wo.ActivityDescription, 1, 5) END AS Task_Code
			, CASE WHEN wo.GroupID = 147 THEN '' ELSE wo.ActivityDescription END AS Task_Description
			, CASE WHEN wo.GroupID = 147 THEN SUBSTRING(wo.ActivityDescription, 1, 3) ELSE '' END AS Activity_Code			
			, CASE WHEN wo.GroupID = 147 THEN wo.ActivityDescription ELSE '' END AS Activity_Description
			, COALESCE( 
				 NULLIF([County Code (FIPS Code)], '')
				,NULLIF([City Code (FIPS Code)], '')
				,NULLIF([Town Code (FIPS Code)],'')
			) AS FIPS 
			, wodf.[AU1 (If applicable)] AS Agency1__AU
			, wodf.[AU2 (Route)] AS Agency2__AU2			
			, wodf.[State Structure No.] as Asset
			, wodf.[FormType]		

FROM 
	reports.WorkOrders AS wo 	
	LEFT JOIN [dbo].[vdot_vw_hmms_WorkOrder_Linear_Default] AS wodf ON wo.ID = wodf.ID














GO

USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_comments]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




 CREATE VIEW [dbo].[vdot_vw_csc_comments] AS 

  SELECT       
            AnnotationId
            ,csc.CSC_SR_ID  
            ,csc.HMMS_SR_ID
            ,subject
            ,NoteText
            ,modifiedOn
            ,duplicateCount 
  FROM   vdot_vw_csc_ALL_comments AS csc
         LEFT JOIN [HMMS].[dbo].[vdot_vw_sr_ALL_comments] vw 
                ON csc.CSC_SR_ID = vw.csc_sr_id COLLATE 
                                   sql_latin1_general_cp1_ci_as 
                   AND csc.notetext = vw.comment COLLATE 
                                      sql_latin1_general_cp1_ci_as 
		INNER JOIN [HMMS].reports.ServiceRequests as hmms_sr 
				on hmms_sr.ID = csc.HMMS_SR_ID COLLATE 
                                      sql_latin1_general_cp1_ci_as
					AND 
					hmms_sr.RefID = csc.CSC_SR_ID COLLATE 
                                      sql_latin1_general_cp1_ci_as
  WHERE  csc.notetext IS NOT NULL 
         AND vw.comment IS NULL 
		 --AND ISNULL(csc.Subject,'') not in ('Comment from AMS','Cancellation Reason', 'Closed Status Reason (To Customer)','Open Status Reason (To Customer)','Response to Customer: (Optional)')
		 


; 


















GO

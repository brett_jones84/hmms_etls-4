USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_districts]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_csc_districts] AS
SELECT distinct
      [districtID]
      ,[districtName]
      ,[districtPhone]
  FROM [HMMS].[dbo].[vdot_vw_csc_residencies]


GO

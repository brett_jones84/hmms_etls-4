USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_sr_ALL_comments]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_sr_ALL_comments] AS
	SELECT * FROM vdot_vw_sr_ALL_comments_SR
	UNION ALL
	SELECT * FROM vdot_vw_sr_ALL_comments_WO
	UNION ALL
	SELECT * FROM vdot_vw_sr_ALL_comments_Dups


GO

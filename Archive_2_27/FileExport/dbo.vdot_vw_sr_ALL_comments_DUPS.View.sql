USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_sr_ALL_comments_DUPS]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_sr_ALL_comments_DUPS] AS
	SELECT 
		wocom.id + ABS(CHECKSUM(NewId())) % 10000  AS ID
		,dups.HMMS_SR_ID AS HMMS_SR_ID
		,dups.CSC_SR_ID	AS CSC_SR_ID
		--,dups.paren4t_ID
		,wocom.WorkOrderID	
		,wocom.CommentType	
		,wocom.Comment	
		,wocom.CommentCreationDate	
		,wocom.CommentUpdateDate
		, 1 AS duplicate
		, dups.parent_ID AS parentHMMSID
		, wocom.CSC_SR_ID AS parentCSCID	
	FROM vdot_vw_hmms_duplicates AS dups
		INNER JOIN vdot_vw_sr_ALL_comments_wo AS wocom ON
			dups.parent_ID = wocom.HMMS_SR_ID
		LEFT JOIN vdot_vw_sr_ALL_comments_sr AS srcom ON
			dups.parent_ID = srcom.hmms_sr_id
			AND
			wocom.comment = srcom.comment
	WHERE srcom.comment IS NULL


GO

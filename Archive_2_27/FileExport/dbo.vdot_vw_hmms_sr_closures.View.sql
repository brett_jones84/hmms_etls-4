USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_hmms_sr_closures]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[vdot_vw_hmms_sr_closures] as
SELECT TOP 50 
	id HMMS_SR_ID
	, refid CSC_SR_ID
	, ClosedDate
	, ClosedByName
	--hardcoded in for IIB integration.
	, 'CLOSED' AS  StatusName

	--add in a STATUS_NAME = 'CLOSED' as a constant.

  FROM [HMMS].[reports].[ServiceRequests]
  --past 5 days
  where closedDate > Dateadd(dd, -5, Getutcdate()) 




GO

USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_A21_departments]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_A21_departments]
AS
SELECT   DepartmentID AS Department_ID, DepartmentName AS Dept_Description
FROM     reports.WorkOrders AS wo
GROUP BY DepartmentID, DepartmentName


GO

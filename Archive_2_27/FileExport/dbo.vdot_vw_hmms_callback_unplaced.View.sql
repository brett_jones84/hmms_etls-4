USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_hmms_callback_unplaced]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_hmms_callback_unplaced] AS
with src as (
SELECT 
	ID	
	,[Duplicate CSC SR Callers]
 FROM reports.WorkOrderForm_MaintenanceLinearWoForm as lwo

 UNION ALL

SELECT 
	ID	
	,[Duplicate CSC SR Callers]
FROM reports.WorkOrderForm_MaintenanceWorkOrderForm as dwo

)

select 
	src.ID as WO_ID	
	,wo.WorkOrderID	
	,wo.ParentID
	,cb.vdot_HMMSIntegrationID	
	,wo.ParentServiceRequestID
	,cb.Callbacks
	,src.[Duplicate CSC SR Callers] 
	
	from src 
	left join reports.WorkOrders as wo on src.ID = wo.id
	left join dbo.vdot_vw_hmms_callback_rollup as cb on wo.ParentServiceRequestID = cb.vdot_HMMSIntegrationID
where cb.Callbacks <> src.[Duplicate CSC SR Callers] 

--select 
--	cb.vdot_HMMSIntegrationID AS SR_ID
--	,cb.Callbacks 
--	,src.[Duplicate CSC SR Callers]
--	,wo.WorkOrderID
--	,wo.ID
-- from dbo.vdot_vw_hmms_callback_rollup as cb
--left join src on cb.vdot_HMMSIntegrationID = src.ID
--left join reports.workorders as wo on cb.vdot_HMMSIntegrationID = wo.ParentServiceRequestID
--where cb.Callbacks <> ISNULL(src.[Duplicate CSC SR Callers], '')


GO

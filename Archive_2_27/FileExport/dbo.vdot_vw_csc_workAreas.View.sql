USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_csc_workAreas]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_csc_workAreas] AS
SELECT [vdot_workareaId] workAreaId
	  ,[vdot_WorkAreaTypeName] workAreaTypeName
      ,[vdot_name] workAreaName
      ,[vdot_AMSWorkAreaID] workAreaAMSId
      ,[vdot_LocationCode] workAreaLocationCode
  FROM [CSC_MSCRM].[dbo].[vdot_workarea] with (NOLOCK);


GO

USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_sr_comments]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_sr_comments] AS
SELECT        
	vw.ID
	, vw.HMMS_SR_ID
	, vw.CSC_SR_ID
	, vw.WorkOrderID
	, vw.CommentType
	, vw.Comment
	, vw.CommentCreationDate
	, vw.CommentUpdateDate
	,sr.vdot_HMMSIntegrationID
	,csc.NoteText
FROM dbo.vdot_vw_sr_ALL_comments as vw
LEFT JOIN dbo.vdot_vw_csc_ALL_comments AS csc 
	ON 
		vw.CSC_SR_ID = csc.CSC_SR_ID COLLATE sql_latin1_general_cp1_ci_as 
		AND
		vw.Comment = csc.NoteText COLLATE sql_latin1_general_cp1_ci_as 
LEFT JOIN dbo.vdot_vw_csc_all_SRs AS sr 
	ON
	vw.HMMS_SR_ID = sr.vdot_HMMSIntegrationID COLLATE sql_latin1_general_cp1_ci_as 
WHERE
	vw.Comment IS NOT NULL
	AND
	csc.NoteText IS NULL
	AND
	sr.vdot_HMMSIntegrationID IS NOT NULL
	AND 
	ISNULL(csc.Subject,'') not in ('Cancellation Reason', 'Closed Status Reason (To Customer)','Open Status Reason (To Customer)','Response to Customer: (Optional)')



GO

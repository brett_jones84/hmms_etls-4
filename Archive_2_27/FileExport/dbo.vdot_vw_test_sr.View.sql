USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_test_sr]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vdot_vw_test_sr] as
select * 
from [TESTDB].[XYZ].[dbo].[VDOT_SERVICEREQUEST_TEMP_ALL] csc
--
--exclude the rows that are already in HMMS
where vdot_srid in
(
	select refid 
	from hmms.reports.ServiceRequests sr
)


GO

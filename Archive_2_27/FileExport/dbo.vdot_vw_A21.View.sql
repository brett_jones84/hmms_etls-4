USE [HMMS]
GO
/****** Object:  View [dbo].[vdot_vw_A21]    Script Date: 2/27/2018 4:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_A21] AS
SELECT   dbo.vdot_vw_A21_Inventory.*
FROM     dbo.vdot_vw_A21_Inventory 

UNION ALL

SELECT   dbo.vdot_vw_A21_Labor.*
FROM     dbo.vdot_vw_A21_Labor 




GO
